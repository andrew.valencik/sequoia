addSbtPlugin("io.github.davidgregory084" % "sbt-tpolecat"  % "0.1.22")
addSbtPlugin("org.scalameta"             % "sbt-scalafmt"  % "2.4.6")
addSbtPlugin("org.scoverage"             % "sbt-scoverage" % "1.9.3")
addSbtPlugin("com.simplytyped"           % "sbt-antlr4"    % "0.8.3")
